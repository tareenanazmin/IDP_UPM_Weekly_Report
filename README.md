# IDP UPM 2021/2022

Name: Tareena Nazmin binti Zainol Ariffin

Matric no.: 198358

Course: EAS 4947 (Aerospace Design Project)

Project: Airship with an agricultural sprayer

## Weekly Report

1. [Week 1](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_1.md)
2. [Week 2](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_2.md)
3. [Week 3](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_3.md)
4. [Week 4](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/week_4.md)
5. [Week 5](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_5.md)
6. [Week 6](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_6.md)
7. [Week 7](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_7.md)
8. [Week 8](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_8.md)
9. [Week 9](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_9.md)
10. [Week 10](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_10.md)
11. [Week 11](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_11.md)
12. [Week 12](https://gitlab.com/tareenanazmin/IDP_UPM_Weekly_Report/-/blob/main/Weekly%20Report/Week_12.md)
