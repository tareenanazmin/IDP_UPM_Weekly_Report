# Week 2

On week 2, a group of students went to H2.1 lab to check out the items needed and find stores that sell the item needed. An equipment checklist is created with links of products.
[Click here](https://docs.google.com/spreadsheets/d/1ij4VMTHH0KphTnuX29gXc1W_kzoVliqFd0tuFXs60F4/edit#gid=80781328) for the spreadsheet

During the second class, Dr Salah has given us our subsystems and divided us into groups that integrate.

The groups are shown below:

![Assigned group](/uploads/4f77660c4056a8da0f93ff6545554bc0/unknown.png) 

The right side is the subsystems group while the left side is the integration between subsystems group. The latter groups were created so that students understand other subsystems' tasks and see the progress from other subsystems and overall progress. 

A realistic project timeline is created with respect for each subsystem from week 1 until week 14 with two milestones in mind. 

| Milestone | Description |
|-----|-----|
| Milestone #1 | Week 8: First controlled flight of Hybrid Airship UAV (using the current design) |
| Milestone #2 | Week 14: New Larger Hybrid Airship UAV with Crop Sprayer operational |


## Note
this weekly log was done one 5/11/2021, but due to technical issue it has to be redone
