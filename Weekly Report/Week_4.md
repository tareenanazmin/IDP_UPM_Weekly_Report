# #04

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 4 |
| Date | 8 Nov 2021 - 12 Nov 2021 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | 'Propulsion and Power' (PnP) subsystems went to lab H2.3 to test the motors in order to get used with the software; RCBenchMark since we will use it regularly. The session went smoothly as it was already planned last wek. Other than that, our leader; Izzat also assign us to read on "Fundamentals of Airship Design Volume 2. |
| Team | This week's coordinator is Hanis, the checker is Ivan, the process monitor is Aienuddin and the recorder is Asfeena. During class, each subsytem shared what they did this week and help to answer questions we had for other subsystems. I was not assigned any role this week. |

# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | What we decided this week is to continue testing available motors as there are 4 motors. We also decided on what to carry out during next session in week 5. |
| Team | During class, we decided to update each subsytem's progress in a general reporsitory under Mr. Azizi's main [repository](https://gitlab.com/putraspace/idp-2021) |

# Justification

| Item | Detail |
| ----- | ----- |
| Subsystem | To get use to the procedure of setting up the motor and running the software with old motors first so that the actual test with new motors and go smoothly and without damaging it (like the one that had tripped before) |
| Team | The general weekly log under Mr. Azizi's repository is so that everyone can stay updated on other susbytem  and for ease when writing report later on. |

# Impact

For me personally, I did not learn much this week because I was unable to attend since I have prior engagement, but as a subsystem, we are able to learn how to operate RCBenchmark and how to setup the thurst test. |

# What could be improved and what is next

| Item | Detail |
| ----- | ----- |
| Subsystem | Since we were unsure of the parameters needed to drive the propeller and the amount of thrust need to work the airship we plan to find the motor's performance next week and read up more. |
| Team | So far everything is okay |

