# #08

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 8 |
| Date | 13 Dec 2021 - 17 Dec 2021 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | 1. Wait for the new motor to arrive on Tuesday. <br> 2. However, we were assured that the thrust test for the new motors did not need to be conducted. |
| Team | This week's coordinator is Hanis, Zaim is the recorder, I'm the checker, while Asfeena is the process monitor as according to this [rooster schedule](https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105). |


# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | To avoid the risk of damaging the motors during testing, we chose not to run thrust test on it. We had considered running the test, but ultimately decided against it as advised. |
| Team | No meeting were held this week because everyone listened want other subsystem did during PIGs and had no question for other subsystem. |

# Justification

| Item | Detail |
| ----- | ----- |
| Subsystem | The reason We opted against running the thrust test since we do not have extra motors and ESC for the real airship incased we damage it prior the flight as we did on the old motors. |
| Team | Each subsystem has presented their part clearly during class eventhough Dr. Salah was not able to join early in the meeting. |

# Impact

We learn to conduct PIGS without our lecturer, however it was a bit chaotic as not a lot of feedback received prior to Dr. Salah's joining.

# What could be improved and what is next

| Item | Detail |
| ----- | ----- |
| Subsystem |  Decided to also carry out the thrust test to find out the motor's actual performance, if it able to drive the airship.  |
| Team | So far everything is okay |

