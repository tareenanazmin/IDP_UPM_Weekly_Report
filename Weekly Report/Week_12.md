#12

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 12 |
| Date | 10 January 2022 - 14 Januray 2022 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | 1. We had to compact our because it changed into a collaborative report with other subsystem. <br> 2. Make some adjustment on the graphs to make it more uniform. <br> 3. Suggested a date on when to launch |
| Team | 1. Helped sprayer subsystem as propulsion has mostly finished their jom. <br> 2. This week's coordinator is Hanis, the recorder is Aienuddin, Aliff is the checker and Zaim acts as the process monitor. I was not assigned any role this week, as according to this [rooster schedule](https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105). |


# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | There were no major decisions made this week |
| Team | Helped other subsystem regarding assembly and improvement. |

# Justification

| Item | Detail |
| ----- | ----- |
| Subsystem | No applicable |
| Team | Propulsion subsystem has finished their work, so the lecturer advise to help other subsystem |

# Impact

Learn to provide support for other subsystem

# What could be improved and what is next

| Item | Detail |
| ----- | ----- |
| Subsystem |  To carry on working on the report.  |
| Team | So far everything is okay |
