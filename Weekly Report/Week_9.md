# #09

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 9 |
| Date | 20 Dec 2021 - 24 Dec 2021 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | Activities were on hold this week and there were positive covid cases in the lab. |
| Team | 1. Work on Resume. <br> 2. Class were postponed this week due to the flood disaster happend that week. <br> 3. This week's coordinator is Hanis, I'm the recorder, Asfeena is the checker, while Ivan is the process monitor as according to this [rooster schedule](https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105). |


# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | Decided to start on writing our report. We will work on it remotely. |
| Team | No meeting were held this week |

# Justification

| Item | Detail |
| ----- | ----- |
| Subsystem | There were not much left for us to do, so we unanimously decided to start on the report. |
| Team | Class were postponed |


# What could be improved and what is next
What could be improved is for people to take SOP seriously.

