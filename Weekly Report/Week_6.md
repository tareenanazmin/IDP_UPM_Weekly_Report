# #06

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 6 |
| Date | 22 Nov 2021 - 26 Nov 2021 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | <div>1. 'Propulsion and Power' (PnP) subsystems went to lab H2.3 to run thrust test with thrust at full throttle. <br>2. Thrust value obtained was 1200gf on 15.8V battery pack and DC Power Supply at 16V. <br> 3. The subsytems had also prepared an [excel sheet](https://onedrive.live.com/view.aspx?resid=28C409A8E53A53FA!28218&ithint=file%2cxlsx&authkey=!AM1100zycYE_Bko) to calculate thrust, power and maximum power available. <br> 4. *Value of cl and cd were given by Simulation Subsystem </div> |
| Team | This week's coordinator is Hanis, the recorder is Aienuddin, Aliff is the checker and Zaim acts as the process monitor. I was not assigned any role this week, as according to this [rooster schedule](https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105). |


# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | The subsystem decides to have a meeting before and after the Friday class. <br> 2. This is to make sure that each member especially those who cannot come to the lab are not clueless on what we have done. <br> 3. Continue running the thrust test to famialiaris ourselve with the system until the new motors arrive.|
| Team | No meeting were held this week. |

# Justification

| Item | Detail |
| ----- | ----- |
| Subsystem | 1. We decided to do a premortem and postmortem so that everyone is on the right track and aren't left behind. <br> 2. Thrust test were conducted for smoother test in the near future and not making careless mistakes|
| Team | Each subsystem has presented their part clearly |

# Impact

I learn that having premortem a day before really helps us getting ready for tomorrow's class because everyone in the subsystem understand it better.

# What could be improved and what is next

| Item | Detail |
| ----- | ----- |
| Subsystem | There were a careless mistake made when running the thrust test; the propeller was installed wrongly causing high vibration and affects the reading. |
| Team | So far everything is okay |

