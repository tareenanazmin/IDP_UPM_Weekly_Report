# #05

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 5 |
| Date | 15 Nov 2021 - 19 Nov 2021 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | 'Propulsion and Power' (PnP) subsystems went to lab H2.3 to run thrust test with available old motors that has the same specification as the ordered (new) motor; [Tarot 4114 320KV](http://www.tarotrc.com/Product/Detail.aspx?Lang=en&Id=7caf622a-6119-4aaf-90e2-ed846acfb13f). The motors condition after running the test is shown in result section below. Other than that, Dr. Ezanee provided a quick explanation of how we can determine the thrust needed from the equilibrium of forces from the airship, as seen in this [note](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/uploads/b97898bac28243ae46bc53ce70aac771/NOTE_1601121.pdf) written by Dr. Ezanee. |
| Team | This week's coordinator is Hanis, the recorder is Ivan, Aienuddin is the checker and Aliff acts as the process monitor. I was not assigned any role this week, as according to this [rooster schedule](https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105). |


# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | We decided to test on all 4 avaiable motor that has the same specification as the one we are ordering from the supplier. To familiarise ourselve with the working sytem of the software to get a consistent result.  |
| Team | No meeting were held this week. |

#Results

| Motor | Condition | Remarks |
| ----- | ----- | ----- |
| 1 | Non-functional | Cannot spin properly |
| 2 | Functioning | Thrust = 1kgf, Current = 4A (not at maximum throttle) |
| 3 | Functioning | Thrust = 0.48kgf, Current = 9A (but also create a lot of vibration) |
| 4 | Burnt | The motor that it used back in week 2/3, because paired it with an unsuitable propeller size |
* all of these motors were tested using a 17in propeller

# Justification

| Item | Detail |
| ----- | ----- |
| Subsystem | To get use to the procedure and gain more pratice so that our job is more easier and has little to no issue when the new motors arrive. |
| Team | Each subsystem has presented their part clearly |

# Impact

From Dr. Ezanee's note, I learn how to calculate the desired drag and thrsut from the equilibrium of forces. 

# What could be improved and what is next

| Item | Detail |
| ----- | ----- |
| Subsystem | To find the right throttle percentage the motor can withstand. |
| Team | So far everything is okay |

