# #07

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 7 |
| Date | 29 Nov 2021 - 3 Dec 2021 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | 1. 'Propulsion and Power' (PnP) subsystems had an online meeting with Dr. Ezanee on 1st Dec 2021 to discuss on the issue faced last week. <br> Issues including: <br> 1. significant vibration on the airship's performance <br> 2. What the values of current can be used for. <br> landing an airship |
| Team | This week's coordinator is Hanis, the recorder is Aliff, Zaim is the checker and I acts as the process monitor. I was not assigned any role this week, as according to this [rooster schedule](https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105). |


# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | There were no major decisions made this week, only small ones like setting the thresholds of current for the working performance |
| Team | No meeting were held this week. |

# Justification

| Item | Detail |
| ----- | ----- |
| Subsystem | To prevent the motor from overworked and later lead to malfunction |
| Team | Each subsystem has presented their part clearly during class |

# Impact

I learn to prepare myself for presentation this week. and crosscheck everything in the main repository with my subsystem

# What could be improved and what is next

| Item | Detail |
| ----- | ----- |
| Subsystem |  To carry out thrust test on the new motor once it is received.  |
| Team | So far everything is okay |

