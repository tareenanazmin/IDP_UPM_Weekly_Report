# #03

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 3 |
| Date | 1 Nov 2021 - 5 Nov 2021 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | A team of 'Propulsion and Power' subsystems when to lab H2.3 to learn how to use the thrust test experimental setup. The lecturer that is present with us is Dr. Ezanee, and he demonstrates how to set up the test and how to use RCBenchMark. During Friday's class, we shared our subsystem's Gantt chart |
| Team | This week's coordinator is Hanis, the checker is Asfeena, the process monitor is Ivan and the recorder is me. The team shared what each subsystem did this week and help to answer questions we had for other subsystems. 

<img src="/uploads/2882d52c287e2684b94789d050ee0c0e/Testing_the_propeller.jpg" alt="Testing the propeller" width="200"/>
<img src="/uploads/6859a579bf925597d3227b04870e8c71/Setting_up_on_the_test.jpg" alt="Setting up the propeller for the test" width="200"/>
<img src="/uploads/5de094014efeec228176fa696e8f8414/Propeller_size.jpg" alt="Propeller size" width="200"/>

# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | During the thrust test, there were two sizes of propeller available for test, one is 17-inch and the other is 23-inch. However, the first test was done using lab H2.3's propeller (15-16-inch). Then the team tested using the 23-inch propeller, unfortunately, the motor could not stand the propeller and trip. The team then decided to not use a 23-inch propeller as it will damage at 50% of throttle.  Later on Friday's class, Lee has decided to present for the Gantt chart |
| Team | Because there are roles in the team, we have decided to have a duty roster where we would rotate the roles every week so that everyone has a chance on being on. Hanis has created a [spread sheet](https://docs.google.com/spreadsheets/u/1/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105) on who is on duty every week.|

# Justification

| Item | Detail |
| ----- | ----- |
| Subsystem | As stated above, when a 23-inch propeller is used, the motor ended up tripping when the throttle barely reaches 50% of the maximum throttle it could achieve. As a safe measure, the team has decided to not operate with a 23-inch propeller with that motor. |
| Team | By rotating the roles every week, everyone has the chance of being the recorder, monitor, and checker |

# Impact

In week 3, I learnt how to set up the propeller on the motor and how to carry out the thrust test. 

# What could be improved and what is next

| Item | Detail |
| ----- | ----- |
| Subsystem | The thrust test could have been better if I had informed *everyone* about the motor specification as I had read it beforehand. Other than that, I think the subsystem should also create a duty roster for who is going to be the presenter during class. As for next week, we plan to run the thrust test *again* with another motor that is available in the propulsion lab (with similar specifications). |
| Team | So far everything is okay |

## Notes

This weekly log was done on 12/11/2021, but the was technical issue
