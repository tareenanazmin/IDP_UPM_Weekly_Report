# #11

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 11 |
| Date | 3 January 2022 - 7 Januray 2022 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | 1. Data analysis for both week 9 and week 10 thrust test data. <br> 2. Continuing to working on the IDP report |
| Team | 1. On monday, had a meeting on discord with other team discussing on each subsytem progress and what they need to do moving on. <br> 2. This week's coordinator is Hanis, the recorder is Ivan, Aienuddin is the checker and Aliff acts as the process monitor. I was not assigned any role this week, as according to this [rooster schedule](https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105). |


# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | There were no major decisions made this week |
| Team | Helped other subsystem regarding measuring, assembly and buying item. |

# Justification

| Item | Detail |
| ----- | ----- |
| Subsystem | No applicable |
| Team | Each subsystem discussed clearly on their part. |

# Impact

Learn to provide support for other subsystem

# What could be improved and what is next

| Item | Detail |
| ----- | ----- |
| Subsystem |  To carry on working on the report.  |
| Team | So far everything is okay |

