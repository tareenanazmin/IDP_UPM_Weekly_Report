# #10

| Item | Details | Item | Detail | Item | Detail |
| ----- | ----- | ----- | ----- | ----- | ----- |
| Name | Tareena | Matric | 198358 | Week | 10 |
| Date | 27 Dec 2021 - 31 Dec 2021 | Subsystem | Propulsion and Power | Group | 2 (T-rex) |

# Agenda and Goals

| Item | Detail |
| ----- | ----- |
| Subsystem | 1. Thrust test data was analysed from data obtained in week 9. <br> 2. The lab was closed as it was during "tempoh bertenang" when one of our friend went there. <br> 3. Continue completeing reports for propulsion and power subsystem. |
| Team | 1. This week's coordinator is Hanis, Asfeena the recorder, while Ivan is the checker and Aienuddin is process monitor as according to this [rooster schedule](https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105). <br> 2. Class were not conducted as it was during 'Tempoh bertenang'  |


# Decisions made and Method

| Item | Detail |
| ----- | ----- |
| Subsystem | Continue working on report to avoid being piled up with other works. |
| Team | No meeting were held this week |

# Justification

It was during 'tempoh bertenang'

